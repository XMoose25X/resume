\documentclass[10pt]{article}
\usepackage[utf8]{inputenc}
% provides \isempty test
\usepackage{xifthen}
% provides \href for hyperlinks
\usepackage{hyperref}
\hypersetup{
    pdfborder = {0 0 0}
}

%\usepackage[light,math]{iwona}
%\usepackage[regular]{roboto}
\usepackage{lmodern} % Allow custom font sizes
% % set font default
\renewcommand*\familydefault{\sfdefault}
% \usepackage[T1]{fontenc}

\usepackage{moresize}
\usepackage{fontawesome5}

% Add optional arguments for commands
\usepackage{xargs}
% Define page styles using geometry
\usepackage[a4paper]{geometry}	
\geometry{top=1cm, bottom=-.6cm, left=0.4cm, right=1cm}

% Less space between header and content
\setlength{\headheight}{-5pt}
% Default indentation is zero
\setlength{\parindent}{0mm}

% Table Layouts
\usepackage{multicol}
\usepackage{multirow}
\usepackage{array}

% Graphics
\usepackage{tikz}
\usetikzlibrary{shapes, backgrounds,mindmap, trees}


% Global Colors
\usepackage{transparent}
\usepackage{color}
\definecolor{complimentaryColor}{RGB}{30, 10, 250} % Dark Blue
\definecolor{backgroundColor}{RGB}{110,110,110} % Dim Grey
\definecolor{arrowColor}{RGB}{250,150,10} % Bright Orange
\definecolor{softFontColor}{RGB}{225,225,225} % Light Grey
\definecolor{primaryColor}{RGB}{0,120,150} % Dark Teal

% returns minipage width minus two times \fboxsep
% to keep padding included in width calculations
\newcommand{\mpwidth}{\linewidth-\fboxsep-\fboxsep}

% Tikz Arrow pointing to the right
% param1: Color of the arrow
\newcommand{\rightArrow}[1]
{\begin{tikzpicture}[scale=0.58]
	\filldraw[fill=#1!100,draw=#1!100!black]	(0,0) -- (0.2,0) -- (0.3,0.2) -- (0.2,0.4) -- (0,0.4) -- (0.1,0.2) -- cycle;
 \end{tikzpicture}
}

% Creates a colored box with graphics as a logical separator of resume content (Experience, Education, etc.)
% param 1: Section Title
\newcommand{\resumeSection}[1]{
	\colorbox{primaryColor}{\parbox{\mpwidth}{
		\vspace{2px}
		\hspace{1pt} \rightArrow{arrowColor} \hspace{-8pt} \rightArrow{arrowColor} \hspace{-8pt} \rightArrow{arrowColor} \textbf{\textcolor{white}{\uppercase{#1}}}
		\vspace{2px}
	}}
}

% Creates an underlined titled for the sidebar
% param 1: Section Title
\newcommand{\sidebarGroup}[1]{
	\begin{center}
		\vspace{4pt}
		\textcolor{white}{\large{\uppercase{#1}}}\\[-4pt]
		\parbox{0.75\mpwidth}{\textcolor{white}{\hrule}}
	\end{center}
}

% Creates a formatted list about a specific event in the resume (building block of main content)
% param 1:	Event Time (e.g. "November 2021 - Present")
% param 2:	Event Name (e.g. "MEPPI")
% param 3:	Location OR Job Title (e.g. "Software Engineer" or "UPJ")
% optional params 4-10:	Content for bullet points
\newcommandx{\resumeEvent}[9][4,5,6,7,8,9]{
	\vspace{5pt}
		% Table with size of mini-page and 3 columns, left-aligned, fill empty space, right aligned (then trim padding)
		\begin{tabular*}{\mpwidth}{l @{\extracolsep{\fill}} r@{}}
			\textcolor{black}{\textbf{#2}} - \textcolor{complimentaryColor}{#3} & \textcolor{backgroundColor}{#1}
		\end{tabular*}
	\textcolor{softFontColor}{\hrule}
	\vspace{6pt}
		\begin{tabular*}{0.5\mpwidth}{p{\mpwidth}}
			\ifx &#4& \else \mbullet \forceindent #4\\[3pt] \fi
			\ifx &#5& \else \mbullet \forceindent #5\\[3pt] \fi
			\ifx &#6& \else \mbullet \forceindent #6\\[3pt] \fi
			\ifx &#7& \else \mbullet \forceindent #7\\[3pt] \fi
			\ifx &#8& \else \mbullet \forceindent #8\\[3pt] \fi
			\ifx &#9& \else \mbullet \forceindent #9\\[3pt] \fi
		\end{tabular*}
	\vspace{3pt}
}

\newcommand{\forceindent}{\hangindent=13px\hangafter=1\indent}
\newcommand{\mbullet}{\vcenteredhbox{\icon{\faCaretRight}[10][softFontColor]}}
\newcommand{\listItem}[2]{\mbullet \forceindent #1 & \textcolor{backgroundColor}{#2} \\
}

% Used to vertically center content
\newcommand*{\vcenteredhbox}[1]{\begingroup
\setbox0=\hbox{#1}\parbox{\wd0}{\box0}\endgroup}


% If you find a good way of including svg without conflicting with other packages you can replace this in the future
\newcommand{\svgIcon}[2]{\def\svgscale{#2}{\input{svgs/#1.pdf_tex}}}
% Creates a box of set size to place an icon into (and colors and sizes it)
% param 1: The icon itself (e.g. "\faHome")
% optional param 2:	Size (defaults to "12")
% optional param 3:	Color (defaults to "white")
\newcommandx{\icon}[3][2=12,3=white]{\makebox(#2, #2){\textcolor{#3}{#1}}}
% Creates an icon and associated text (and colors and sizes them)
% param 1: The icon itself (e.g. "\faHome")
% param 2: The text (e.g. "Pittsburgh")
% optional param 3:	Size (defaults to "12")
% optional param 4:	Color (defaults to "white")
\newcommandx{\iconText}[4][3=12,4=white]{\vcenteredhbox{\icon{#1}[#3][#4]} \vcenteredhbox{\textcolor{#4}{#2}}}

%============================================================================%
%                                                                            %
%                            Resume Content                                  %
%                                                                            %
%============================================================================%

\begin{document}
	\colorbox{primaryColor}{
		%============================================================================%
		%                            Sidebar Content                                 %
		%============================================================================%
		\begin{minipage}[c][0.98\textheight][t]{0.24\linewidth}
			\begin{flushleft}
				\setlength\parindent{8pt}
				\setlength\multicolsep{-8pt}
			 	\setlength\columnsep{2pt}
				\vspace{4pt}
				\sidebarGroup{Contact}
					\iconText{\faHome}{Pittsburgh, PA} \\[6pt]
					\iconText{\faPhone}{814-475-2731} \\[6pt]
					\iconText{\faEnvelope}{\href{mailto:mathew.moose95@gmail.com}{mathew.moose95@gmail}} \\[6pt]
					\iconText{\faMousePointer}{\href{https://moose.software}{moose.software}} \\[6pt]
					\iconText{\faLinkedinIn}{\href{https://www.linkedin.com/in/mathew-moose/}{in/mathew-moose}} \\[6pt]
				
				\sidebarGroup{Languages}
					\begin{multicols}{2}
						\iconText{\faJava}{Java} \\[6pt]
						\iconText{\svgIcon{CSharp}{0.65}}{C\#} \\[6pt]
						\iconText{\faPython}{Python} \\[6pt]
						\iconText{\faGem}{Ruby} \\[6pt]
						\iconText{\faDatabase}{SQL} \\[6pt]
					\columnbreak
					\setlength\parindent{0pt}
						\iconText{\svgIcon{Typescript}{0.2}}{TypeScript} \\[6pt]
						\iconText{\faJs}{JavaScript} \\[6pt]
						\iconText{\faCss3}{CSS} \\[6pt]
						\iconText{\faHtml5}{HTML} \\[6pt]
					\end{multicols}

				\sidebarGroup{Technologies}
					\vspace{-28pt}
					\textcolor{white}{\begin{center}\footnotesize{Frameworks}\end{center}}
					\setlength\multicolsep{-6pt}
					\begin{multicols}{2}
						\iconText{\faReact}{React} \\[6pt]
						\iconText{\faAngular}{Angular} \\[6pt]
						\iconText{\faVuejs}{Vue} \\[6pt]
					\columnbreak
					\setlength\parindent{0pt}
						\iconText{\faLeaf}{Spring} \\[6pt]
						\iconText{\svgIcon{Hibernate}{0.65}}{Hibernate} \\[6pt]
						\iconText{\faSitemap}{EFCore} \\[6pt]
					\end{multicols}

					\vspace{-1pt}
					\textcolor{white}{\begin{center}\footnotesize{Testing}\end{center}}
					\setlength\multicolsep{-6pt}
					\begin{multicols}{2}
						\iconText{\svgIcon{XUnit}{0.65}}{XUnit} \\[6pt]
						\iconText{\svgIcon{PITest}{0.65}}{PITest} \\[6pt]
						\iconText{\svgIcon{Selenium}{0.65}}{Selenium} \\[6pt]
					\columnbreak
					\setlength\parindent{0pt}
						\iconText{\svgIcon{Junit}{0.65}}{JUnit} \\[6pt]
						\iconText{\svgIcon{Jest}{0.65}}{Jest} \\[6pt]
						\iconText{\svgIcon{Cucumber}{0.65}}{Cucumber} \\[6pt]
					\end{multicols}

					\vspace{-1pt}
					\textcolor{white}{\begin{center}\footnotesize{CI/CD}\end{center}}
					\setlength\multicolsep{-6pt}
					\begin{multicols}{2}
						\iconText{\faJenkins}{Jenkins} \\[6pt]
						\iconText{\svgIcon{Gradle}{0.65}}{Gradle} \\[6pt]
						\iconText{\svgIcon{Ant}{1}}{Ant} \\[6pt]
					\columnbreak
					\setlength\parindent{0pt}
						\iconText{\faGit*}{Git} \\[6pt]
						\iconText{\faDocker}{Docker} \\[6pt]
						\iconText{\svgIcon{Kubernetes}{0.65}}{Kubernetes} \\[6pt]
					\end{multicols}

					\vspace{-1pt}
					\textcolor{white}{\begin{center}\footnotesize{Tools}\end{center}}
					\setlength\multicolsep{-6pt}
					\begin{multicols}{2}
						\iconText{\svgIcon{Kafka}{0.65}}{Kafka} \\[6pt]
						\iconText{\faAws}{AWS} \\[6pt]
						\iconText{\svgIcon{Jetbrains}{0.65}}{JetBrains} \\[6pt]
						\iconText{\faUnity}{Unity} \\[6pt]
					\columnbreak
					\setlength\parindent{0pt}
						\iconText{\svgIcon{RabbitMQ}{0.6}}{RabbitMQ} \\[6pt]
						\iconText{\faJira}{Jira} \\[6pt]
						\iconText{\faSourcetree}{SourceTree} \\[6pt]
					\end{multicols}

					\vspace{-1pt}
					\textcolor{white}{\begin{center}\footnotesize{Operating Systems}\end{center}}
					\setlength\multicolsep{-6pt}
					\begin{multicols}{2}
						\iconText{\faWindows}{Windows} \\[6pt]
						\iconText{\faApple}{Mac} \\[6pt]
					\columnbreak
					\setlength\parindent{0pt}
						\iconText{\faLinux}{Linux} \\[6pt]
					\end{multicols}

				\vspace{30pt}
				\sidebarGroup{About Me}
					\vspace{-10pt}
					\LARGE{
						\hspace{6pt}\icon{\faGamepad}[20]
						\icon{\faTv}[20]
						\icon{\faHeadphones}[20]
						\icon{\faBicycle}[20]
						\icon{\svgIcon{Whistle}{1}}[20]\\
						\hspace{3pt}\svgIcon{Funko}{0.12}
						\svgIcon{PokemonCards}{0.05}
						\svgIcon{Lego}{1.15}
					}

				\vspace*{\fill}
				\setlength\parindent{0pt}
				\textcolor{white}{
					\footnotesize{View resume source code:\\[-10pt] \href{https://bitbucket.org/XMoose25X/resume}{bitbucket.org/XMoose25X/resume}
					}
				}
			\end{flushleft}
		\end{minipage}
	}\colorbox{white}{
		%============================================================================%
		%                               Main Content                                 %
		%============================================================================%
		\begin{minipage}[c][0.98\textheight][t]{0.75\linewidth}
			\vspace{-3pt}
			\colorbox{backgroundColor}{
				\makebox[\mpwidth-\fboxsep-\fboxsep-0.5pt][c]{
					\HUGE{\textcolor{white}{\uppercase{Mathew Moose}}}
					\textcolor{primaryColor}{\rule[-1mm]{1mm}{0.9cm}}
					\parbox[c]{5cm}{\vspace{-4px}
						\large{\textcolor{white}{\uppercase{Software Architect}}}
					}
				}
			}

			\resumeSection{Summary}

				Dynamic and results-oriented technical leader with 10\raisebox{0.15ex}{+} years of experience in industry and education.\\
				Proven track record of delivering innovative solutions that consistently exceed client expectations.\\
				Passionate mentor and team builder, dedicated to guiding diverse, cross-functional teams through complex challenges to achieve exceptional outcomes.

			\vspace{12pt}
			\resumeSection{Experience}

				\resumeEvent
				{November 2021 - Present}
				{Mitsubishi Electric Power Products Inc.}
				{Software Architect}
				[Successfully launched a software department, establishing foundational processes and tools, in order to fuel a multimillion-dollar product.]
				[Designed an extensible multi-tier application for cloud and on-premise solutions that drives the operations of the largest utility companies in North America.]
				[Implemented a Jenkins development pipeline from scratch, including scalable Docker agents, artifact hosting, static analysis scanners, and automated deployments.]
				[Utilized WebRTC to create secure video streams from cameras on disparate networks in order to monitor system failures in real time.]
				[Integrated with multiple sensor platforms, including Teledyne FLIR thermal cameras, Skydio drones, and Boston Dynamics Spot robots for processing visual and acoustic readings.]

				\resumeEvent
				{May 2019 - November 2021}
				{McKesson}
				{Software Engineering Manager}
				[Established a team for developer efficiency that helped reduce application build times by 75\%, saving the company half a million dollars annually.]
				[Migrated 10,000\raisebox{0.15ex}{+} lines of code from monolithic to microservices architecture, boosting scalability and enabling monthly updates instead of annual ones, giving the company a competitive edge.]
				[Transferred 4 million lines of code from Subversion into a rebasing Gitflow model, reducing mean time for defect removal by 200\%.]
				[Implemented a vaccination workflow that decreased pharmacist administration time by 30\% during the onset of the COVID-19 pandemic, saving millions of labor hours.]
				[Standardized coding guidelines and practices for a team of over 40 developers, then implemented static analysis and linting tools to assist in the maintainability of the codebase.]
				% [Revitalized testing initiatives by modernizing tools and creating an integration test framework, bringing integration testing to our business unit's flagship product.]

				\resumeEvent
				{January 2018 - May 2019}
				{Aerotech}
				{Software Engineer}
				[Led cross-team communication for ISO-9001 certification, improving quality standards and strengthening B2B relationships with prospective customers.]
				[Replaced proprietary InstallShield installations with customized WiX solutions, leading to business savings and better overall user experience.]
				[Designed software to automate CAD drawings for system engineers, saving over \$100,000 anually.]
				[Architected a solution for generating system configurations, eliminating over 150 hours annually.]

				\resumeEvent
				{January 2017 - December 2017}
				{Problem Solutions}
				{Software Engineer}
				[Accelerated delivery of a product for a Fortune 500 client, completing the contract 2 months ahead of schedule.]
				[Maintained the network infrastructure and facilitated repairs for a team of over 50 developers.]
				[Developed web-based applications using .NET, React, and other notable technologies.]
				[Collaborated with vendors and other internal teams in order to develop rigorous unit, smoke and acceptance testing for all of our written software.]

				% \resumeEvent
				% {July 2015 - December 2017}
				% {Geek Squad}
				% {Quality Assurance Agent}
				% [Navigated a team of technical agents through a reorganization effort with guided workshops.]
				% [Oversaw the quality assurance of third-party remote support teams of over 600 global employees.]
				% [Established an Employee Engagement Team that influenced a positive, collaborative culture.]

			\vspace{2pt}
			\resumeSection{Education}

				\resumeEvent
				{Graduated December 2017}
				{B.S. Computer Science}
				{University of Pittsburgh at Johnstown}
				[Mathematics Minor - 3.96 GPA - Summa Cum Laude]
				[Phi Kappa Phi, Phi Eta Sigma, University Scholar, Dean's List]

			\vspace{2pt}
			\resumeSection{Certifications and Awards}

				\begin{tabular*}{\mpwidth}{l @{\extracolsep{\fill}} r @{}}
					\listItem{Certified SAFe\textsuperscript{\textregistered} SP 4 Practitioner}{August 2019}
					\listItem{MRxTs Innovation Days Winner}{September 2019}
					\listItem{MPS Care Model Employee Spotlight}{November 2020}
					\listItem{FAA Remote Pilot Certificate (Part 107) (sUAS Commercial)}{August 2023}

				\end{tabular*}

		\end{minipage}
	}
\end{document}