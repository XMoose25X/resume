# Resume

Mathew Moose's personal resume using LaTeX.

## Prerequisites

* Install a [TeX Distribution](https://www.latex-project.org/get/#tex-distributions)
    * It must include [pdflatex](https://linux.die.net/man/1/pdflatex)
* Alternatively, install [Docker](https://docs.docker.com/)

## Running

### Docker (Recommended)
This is a simplified version of the [Atlassian Guide](https://support.atlassian.com/bitbucket-cloud/docs/debug-pipelines-locally-with-docker/) to running pipelines locally. It assumes you are using [Docker](https://docs.docker.com/) on Windows but paths can be tweaked for other operating systems.

1. Make sure docker is running
1. Download the latest image from DockerHub and run it from the root of your repository.
    * `docker run -it --volume=%cd%:/resume --workdir="/resume" --memory=4g --memory-swap=4g --memory-swappiness=0 --entrypoint=/bin/bash xmoose25x/latex-builder:latest`
1. Perform the commands executed by the `bitbucket.pipelines.yml` file from within the Docker container CLI.
    * ```bash
        pdflatex -aux-directory=pdflatex_aux -jobname=Mathew_Moose_Resume Resume.tex
      ```

## Local Live Compilation using VSCode

1. Install [LaTeX Workshop plugin](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop).
1. Follow the pre-requisites of the plugin (the below steps are based on 8.6.0).
    * Include LaTeX in PATH
    * Have `latexmk` included in your distribution
    * Install [Perl](https://www.perl.org/get.html)
        * Use [Strawberry Perl](http://strawberryperl.com/) for Windows
1. Use the **Build LaTeX project** and **View LaTeX PDF File** buttons that appears in the upper-right corner when editing your `.tex` files.

*Note:* `.vscode/settings.json` contains all necessary workspace settings for ease-of-use.

## Continuous Integration and Deployment

As it stands, this project includes a `bitbucket-pipelines.yml` file that automatically builds the LaTeX PDFs and deploys the files to BitBucket's downloads pages.

Pipelines can be ignored for any commits that are pushed with `[skip ci]` within the commit message.

### SVG Creation
`inkscape -D NET_logo.svg  -o NET_logo.pdf --export-latex`
